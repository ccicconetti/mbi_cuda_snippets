# FFT benchmark

This directory contains the results obtained with the FFT benchmark.

## Setup

### Hardware

Two different hosts have been used:

* An NVIDIA Jetson TX2 evaluation board.
* A dual-socket Intel Xeon E5-2660 server with NVIDIA Telsa K80 and Telsa P100 GPUs.

### Software

* On the Jetson TX2: CUDA toolkit 8.0.64.
* On the Intel server: CUDA toolkit 8.0.27 (stable) and 9.0.69_384.27 (early access).

## Methodology

The performance is measured in terms of the average time required to perform 1000 FFTs with a given size (only power of two values have been used).
For every FFT size the experiment is repeated until statistical convergence is reached, estimated using 95% t-Student confidence intervals.
FFT sizes from 2^7 to 2^20 have been benchmarked.


## Results

Can be found in the following directories: cuda8_k80, cuda8_p100, cuda9_k80, cuda9_p100, jetson_tx2.

Three different plots are provided:

* fft1: comparison between Jetson TX2 and Telsa P100, with both FP16 and FP32
* fft2: comparison between CUDA8 and CUDA9 with Telsa P100, FP32 only
* fft3: comparison between CUDA8 and CUDA9 with Telsa K80, FP32 only
