#pragma once

class Chronometer {
 public:
  Chronometer() : theStart(), theStop(), theElapsedMs(0.0f), theStopped(false) {
    cudaError_t myRet = cudaEventCreate(&theStart);
    assert(myRet == cudaSuccess);

    myRet = cudaEventCreate(&theStop);
    assert(myRet == cudaSuccess);

    myRet = cudaEventRecord(theStart, 0);
    assert(myRet == cudaSuccess);
  }

  // return elapsed time in milliseconds
  float stop() {
    if (not theStopped) {
      cudaError_t myRet = cudaEventRecord(theStop, 0);
      assert(myRet == cudaSuccess);

      myRet = cudaEventSynchronize(theStop);
      assert(myRet == cudaSuccess);

      myRet = cudaEventElapsedTime(&theElapsedMs, theStart, theStop);
      assert(myRet == cudaSuccess);

      theStopped = true;
    }

    return theElapsedMs;
  }

  ~Chronometer() {
    cudaEventDestroy(theStart);
    cudaEventDestroy(theStop);
  }

 private:
  cudaEvent_t theStart;
  cudaEvent_t theStop;
  float theElapsedMs;
  bool theStopped;
};
