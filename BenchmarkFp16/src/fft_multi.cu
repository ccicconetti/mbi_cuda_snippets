// Copyright (c) 2017-2022 M.B.I srl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//   CONTACT-INFO  Massimiliano Ghilardi
//
//   Postal: MBI S.r.l.
//   Via F. Squartini, 6
//   I56121 - Pisa
//   Italy
//
//   Email:  mghilardi@mbigroup.it

#include <cassert>
#include <iostream>

#include <cuda_fp16.h>
#include <cufft.h>
#include <cufftXt.h>

#include "chronometer.h"
#include "devicevector.h"
#include "hostvector.h"

static const size_t MIN_SIZE = 1 << 10;
static const size_t MAX_SIZE = 1 << 20;

static const size_t NUM_REPETITIONS = 4096;

#define BLOCK_SIZE (1 << 9)

enum FpType { FP32 = 0, FP16 = 1 };

// kernel to convert FP32->FP16
__global__ void KernelConvertFloat2ToHalf2(const float2* aIn, half2* aOut,
                                           const unsigned aSize) {
  const unsigned myPos = blockIdx.x * blockDim.x + threadIdx.x;
  if (myPos < aSize) {
    aOut[myPos] = __float22half2_rn(aIn[myPos]);
  }
}

int main() {
  // select compute capability >= 6.0 (Pascal)
  cudaDeviceProp myProp;
  int myDev;

  cudaGetDevice(&myDev);
  memset(&myProp, 0, sizeof(cudaDeviceProp));
  myProp.major = 6;
  myProp.minor = 0;
  const auto myResultSetDevice = cudaChooseDevice(&myDev, &myProp);
  assert(myResultSetDevice == cudaSuccess);

  cudaDeviceProp myPropTest;
  cudaGetDeviceProperties(&myPropTest, myDev);

  std::cout << "# Compute capability: " << myPropTest.major << '.'
            << myPropTest.minor << std::endl;

  cudaSetDevice(myDev);

  const size_t myY = 120;
  const size_t myVectorLen = MAX_SIZE * myY;

  HostVector myHostVector(myVectorLen);
  DeviceVector<float2> myVector(myVectorLen);

  for (FpType myFp : {FP32, FP16}) {
    std::cout << "# benchmark FFT_MULTI " << (myFp == FP32 ? "FP32" : "FP16")
              << ", batch size = " << myY << std::endl;
    std::cout << "# FFT size, time (ms), speed (elements/second)" << std::endl;

    DeviceVector<float2> myResult(myFp == FP32 ? myVectorLen : 0);
    DeviceVector<half2> myVectorFp16(myFp == FP16 ? myVectorLen : 0);
    DeviceVector<half2> myResultFp16(myFp == FP16 ? myVectorLen : 0);

    // FFT size = mySize
    for (long long mySize = MIN_SIZE; mySize <= MAX_SIZE; mySize <<= 1) {
      const size_t myRingSize = MAX_SIZE / mySize;
      int myStrideX = 1;
      int myStrideY = mySize;

      // create FFT plan
      cufftHandle myPlan;
      if (myFp == FP16) {
        cufftResult myErr = cufftCreate(&myPlan);
        assert(myErr == CUFFT_SUCCESS);

        long long myEmbed = mySize;
        size_t myWorkSize = 0;
        myErr = cufftXtMakePlanMany(myPlan,
                                    1,          // rank
                                    &mySize,    // FFT size
                                    &myEmbed,   // must be {aLength} for 1-D FFT
                                    myStrideX,  // input vector stride
                                    myStrideY,  // input batch distance
                                    CUDA_C_16F,  // input type = half2
                                    &myEmbed,   // must be {aLength} for 1-D FFT
                                    myStrideX,  // output vector stride
                                    myStrideY,  // output batch distance
                                    CUDA_C_16F,   // output type = half2
                                    myY,          // batch size = # FFT
                                    &myWorkSize,  // work area size
                                    CUDA_C_16F);  // algo type = half2
        assert(myErr == CUFFT_SUCCESS);

      } else {
        assert(myFp == FP32);

        int mySizeInt = int(mySize);
        int myEmbedInt = mySizeInt;

        const cufftResult myErr =
            cufftPlanMany(&myPlan,
                          1,            // rank
                          &mySizeInt,   // FFT size
                          &myEmbedInt,  // must be {aLength} for 1-D FFT
                          myStrideX,    // step of input data
                          myStrideY,  // distance between batches of input data
                          &myEmbedInt,  // must be {aLength} for 1-D FFT
                          myStrideX,    // step of output data
                          myStrideY,  // distance between batches of output data
                          CUFFT_C2C,  //
                          myY);       // batch size = # FFT

        assert(myErr == CUFFT_SUCCESS);
      }

      myHostVector.randomFill(1e-3f);
      cudaMemcpy(myVector.data(), myHostVector.data(), sizeof(float2) * mySize,
                 cudaMemcpyHostToDevice);

      float myElapsedMs = 0.0f;

      if (myFp == FP32) {
        // wait for setup to complete
        cudaDeviceSynchronize();

        Chronometer myChrono;

        for (size_t myTest = 0; myTest < NUM_REPETITIONS; ++myTest) {
          // select random offset in the input
          const size_t myOffset =
              myRingSize == 1 ? 0
                              : lrand48() % (mySize * myY * (myRingSize - 1));
          // calculate FFT transform
          const cufftResult myErr = cufftExecC2C(
              myPlan,
              reinterpret_cast<cufftComplex*>(myVector.data() + myOffset),
              reinterpret_cast<cufftComplex*>(myResult.data() + myOffset),
              CUFFT_FORWARD);
          assert(myErr == CUFFT_SUCCESS);
        }
        myElapsedMs = myChrono.stop();
      } else {
        assert(myFp == FP16);

        // call kernel to convert FP32 to FP16
        {
          dim3 myGrid((myVectorLen + BLOCK_SIZE - 1) / BLOCK_SIZE);
          dim3 myBlock(BLOCK_SIZE);
          KernelConvertFloat2ToHalf2<<<myGrid, myBlock>>>(
              myVector.data(), myVectorFp16.data(), myVectorLen);
        }
        // wait for setup to complete
        cudaDeviceSynchronize();

        Chronometer myChrono;

        for (size_t myTest = 0; myTest < NUM_REPETITIONS; ++myTest) {
          // select random offset in the input
          const size_t myOffset =
              myRingSize == 1 ? 0
                              : lrand48() % (mySize * myY * (myRingSize - 1));
          // calculate FFT transform
          const cufftResult myErr =
              cufftXtExec(myPlan,                          //
                          myVectorFp16.data() + myOffset,  //
                          myResultFp16.data() + myOffset,  //
                          CUFFT_FORWARD);
          assert(myErr == CUFFT_SUCCESS);
        }
        myElapsedMs = myChrono.stop();
      }
      const float myAvgTimeMs = myElapsedMs / NUM_REPETITIONS;
      const float myAvgSpeed = (mySize * myY) / myAvgTimeMs * 1e3f;
      std::cout << '\t' << mySize << '\t' << myAvgTimeMs << '\t' << myAvgSpeed
                << std::endl;

      // Destroy cuFFT context
      cufftDestroy(myPlan);
    }
  }

  return 0;
}
