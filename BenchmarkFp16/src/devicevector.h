#pragma once

#include <cassert>
#include <cstddef>  // size_t

#include <cuda_runtime_api.h>

template <class T>
class DeviceVector {
 public:
  explicit DeviceVector(const size_t aSize)  //
      : thePointer(nullptr),                 //
        theSize(0)                           //
  {
    if (aSize != 0) {
      cudaError_t myErr = cudaMalloc((void**)&thePointer, sizeof(T) * aSize);
      assert(myErr == cudaSuccess);
      (void)myErr;
    }
    theSize = aSize;
  }

  ~DeviceVector() {
    if (thePointer) {
      cudaError_t myErr = cudaFree(thePointer);
      assert(myErr == cudaSuccess);
      (void)myErr;
    }
  }

  T* data() { return thePointer; }
  const T* data() const { return thePointer; }
  size_t size() const { return theSize; }

 private:
  T* thePointer;
  size_t theSize;
};
