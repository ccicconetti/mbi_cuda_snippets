// Copyright (c) 2017-2022 M.B.I srl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//   CONTACT-INFO  Massimiliano Ghilardi
//
//   Postal: MBI S.r.l.
//   Via F. Squartini, 6
//   I56121 - Pisa
//   Italy
//
//   Email:  mghilardi@mbigroup.it

#include <cassert>
#include <cmath>
#include <iostream>

#include <cuda_fp16.h>

#include "chronometer.h"
#include "devicevector.h"
#include "hostvector.h"

static const size_t MIN_SIZE = 1 << 12;
static const size_t MAX_SIZE = 1 << 25;

static const size_t NUM_REPETITIONS = 4096;

#define BLOCK_SIZE (1 << 9)

// kernel to convert FP32->FP16
__global__ void KernelConvertFloat2ToHalf2(const float2* aIn, half2* aOut,
                                           const unsigned aSize) {
  const unsigned myPos = blockIdx.x * blockDim.x + threadIdx.x;
  if (myPos < aSize) {
    aOut[myPos] = __float22half2_rn(aIn[myPos]);
  }
}

// kernel to perform the reduction with FP32
template <unsigned blockSize>
__global__ void KernelReductionFp32(const float2* g_idata, float2* g_odata,
                                    unsigned n) {
  extern __shared__ float2 shared_float2[];

  unsigned tid = threadIdx.x;
  unsigned i = blockIdx.x * blockSize * 2 + threadIdx.x;
  unsigned gridSize = blockSize * 2 * gridDim.x;

  shared_float2[tid].x = 0;
  shared_float2[tid].y = 0;

  while (i < n) {
    shared_float2[tid].x += g_idata[i].x;
    shared_float2[tid].y += g_idata[i].y;

    if (i + blockSize < n) {
      shared_float2[tid].x += g_idata[i + blockSize].x;
      shared_float2[tid].y += g_idata[i + blockSize].y;
    }
    i += gridSize;
  }

  __syncthreads();

  if ((blockSize >= 1024) && (tid < 512)) {
    shared_float2[tid].x += shared_float2[tid + 512].x;
    shared_float2[tid].y += shared_float2[tid + 512].y;
  }
  __syncthreads();

  if ((blockSize >= 512) && (tid < 256)) {
    shared_float2[tid].x += shared_float2[tid + 256].x;
    shared_float2[tid].y += shared_float2[tid + 256].y;
  }
  __syncthreads();

  if ((blockSize >= 256) && (tid < 128)) {
    shared_float2[tid].x += shared_float2[tid + 128].x;
    shared_float2[tid].y += shared_float2[tid + 128].y;
  }
  __syncthreads();

  if ((blockSize >= 128) && (tid < 64)) {
    shared_float2[tid].x += shared_float2[tid + 64].x;
    shared_float2[tid].y += shared_float2[tid + 64].y;
  }
  __syncthreads();

  if (tid < 32) {
    if (blockSize >= 64) {
      shared_float2[tid].x += shared_float2[tid + 32].x;
      shared_float2[tid].y += shared_float2[tid + 32].y;
    }
    for (int offset = warpSize / 2; offset > 0; offset /= 2) {
      shared_float2[tid].x +=
          __shfl_down_sync(0xFFFFFFFF, shared_float2[tid].x, offset);
      shared_float2[tid].y +=
          __shfl_down_sync(0xFFFFFFFF, shared_float2[tid].y, offset);
    }
  }

  if (tid == 0) {
    g_odata[blockIdx.x].x = shared_float2[0].x;
    g_odata[blockIdx.x].y = shared_float2[0].y;
  }
}

///// kernel to perform the reduction with FP16
template <unsigned blockSize>
__global__ void KernelReductionFp16(const half2* g_idata, half2* g_odata,
                                    unsigned n) {
  extern __shared__ half2 shared_half2[];

  unsigned tid = threadIdx.x;
  unsigned i = blockIdx.x * blockSize * 2 + threadIdx.x;
  unsigned gridSize = blockSize * 2 * gridDim.x;
  shared_half2[tid] = __floats2half2_rn(0, 0);

  while (i < n) {
    shared_half2[tid] = __hadd2(shared_half2[tid], g_idata[i]);

    if (i + blockSize < n) {
      shared_half2[tid] = __hadd2(shared_half2[tid], g_idata[i + blockSize]);
    }

    i += gridSize;
  }

  if ((blockSize >= 1024) && (tid < 512)) {
    shared_half2[tid] = __hadd2(shared_half2[tid], shared_half2[tid + 512]);
  }

  __syncthreads();

  if ((blockSize >= 512) && (tid < 256)) {
    shared_half2[tid] = __hadd2(shared_half2[tid], shared_half2[tid + 256]);
  }

  __syncthreads();

  if ((blockSize >= 256) && (tid < 128)) {
    shared_half2[tid] = __hadd2(shared_half2[tid], shared_half2[tid + 128]);
  }

  __syncthreads();

  if ((blockSize >= 128) && (tid < 64)) {
    shared_half2[tid] = __hadd2(shared_half2[tid], shared_half2[tid + 64]);
  }

  __syncthreads();

  if (tid < 32) {
    if (blockSize >= 64) {
      shared_half2[tid] = __hadd2(shared_half2[tid], shared_half2[tid + 32]);
    }
    for (int offset = warpSize / 2; offset > 0; offset /= 2) {
      shared_half2[tid] =
          __hadd2(shared_half2[tid],
                  __shfl_down_sync(0xFFFFFFFF, shared_half2[tid], offset));
    }
  }

  if (tid == 0) {
    g_odata[blockIdx.x] = shared_half2[0];
  }
}

/////////////////////////////////////////////////////////////////////////////
//////////////////  BENCHMARK
//////////////////////////////////////////////////////////////////////////////

int main() {
  // select compute capability 6.0 (Pascal)
  cudaDeviceProp myProp;
  int myDev;

  cudaGetDevice(&myDev);
  memset(&myProp, 0, sizeof(cudaDeviceProp));
  myProp.major = 6;
  myProp.minor = 0;
  const cudaError_t myResultSetDevice = cudaChooseDevice(&myDev, &myProp);
  assert(myResultSetDevice == cudaSuccess);

  cudaDeviceProp myPropTest;
  cudaGetDeviceProperties(&myPropTest, myDev);

  std::cout << "# Compute capability: " << myPropTest.major << '.'
            << myPropTest.minor << std::endl;

  cudaSetDevice(myDev);

  // USE FP32
  std::cout << "# benchmark REDUCE FP32" << std::endl;
  std::cout << "# size, time (ms), speed (elements/second)" << std::endl;

  for (size_t mySize = MIN_SIZE; mySize <= MAX_SIZE; mySize <<= 1) {
    const size_t mySizePadded = 5 * mySize;

    const size_t myNumBlocksReduction = 64u;
    HostVector myHostVector(mySizePadded);
    DeviceVector<float2> myVector(mySizePadded);
    DeviceVector<float2> myResult(myNumBlocksReduction);

    myHostVector.randomFill(1e-3f);

    cudaMemcpy(myVector.data(), myHostVector.data(),
               sizeof(float2) * mySizePadded, cudaMemcpyHostToDevice);

    // wait for setup to complete
    cudaDeviceSynchronize();

    // kernel parameters
    const unsigned myNumThreads = BLOCK_SIZE;
    const unsigned mySharedMemSize = myNumThreads * sizeof(float2);
    dim3 myGrid(myNumBlocksReduction);
    dim3 myBlock(myNumThreads);

    Chronometer myChrono;

    for (size_t myTest = 0; myTest < NUM_REPETITIONS; ++myTest) {
      const size_t myOffset = lrand48() % (mySizePadded - mySize);

      KernelReductionFp32<myNumThreads><<<myGrid, myBlock, mySharedMemSize>>>(
          myVector.data() + myOffset, myResult.data(), mySize);
    }
    const float myElapsedTimeMs = myChrono.stop();
    const float myAvgTimeMs = myElapsedTimeMs / NUM_REPETITIONS;
    const float myAvgSpeed = mySize / myAvgTimeMs * 1e3f;
    std::cout << '\t' << mySize << '\t' << myAvgTimeMs << '\t' << myAvgSpeed
              << std::endl;
  }

  // USE FP16
  std::cout << "# benchmark REDUCE FP16" << std::endl;
  std::cout << "# size, time (ms), speed (elements/second)" << std::endl;

  for (size_t mySize = MIN_SIZE; mySize <= MAX_SIZE; mySize <<= 1) {
    const size_t mySizePadded = 5 * mySize;

    const size_t myNumBlocksReduction = 64u;
    HostVector myHostVector(mySizePadded);
    DeviceVector<float2> myVector(mySizePadded);
    DeviceVector<half2> myVectorFp16(mySizePadded);
    DeviceVector<half2> myResultFp16(myNumBlocksReduction);

    myHostVector.randomFill(1e-3f);

    cudaMemcpy(myVector.data(), myHostVector.data(),
               sizeof(float2) * mySizePadded, cudaMemcpyHostToDevice);

    // call kernel to convert FP32 to FP16
    {
      dim3 myGrid((mySizePadded + BLOCK_SIZE - 1) / BLOCK_SIZE);
      dim3 myBlock(BLOCK_SIZE);
      KernelConvertFloat2ToHalf2<<<myGrid, myBlock>>>(
          myVector.data(), myVectorFp16.data(), mySizePadded);
    }
    // wait the conversion 32->16
    cudaDeviceSynchronize();

    // kernel parameters
    const unsigned myNumThreads = BLOCK_SIZE;
    const unsigned mySharedMemSize = myNumThreads * sizeof(half2);
    dim3 myGrid(myNumBlocksReduction);
    dim3 myBlock(myNumThreads);

    Chronometer myChrono;

    for (size_t myTest = 0; myTest < NUM_REPETITIONS; ++myTest) {
      const size_t myOffset = lrand48() % (mySizePadded - mySize);

      KernelReductionFp16<myNumThreads><<<myGrid, myBlock, mySharedMemSize>>>(
          myVectorFp16.data() + myOffset, myResultFp16.data(), mySize);
    }
    const float myElapsedTimeMs = myChrono.stop();
    const float myAvgTimeMs = myElapsedTimeMs / NUM_REPETITIONS;
    const float myAvgSpeed = mySize / myAvgTimeMs * 1e3f;
    std::cout << '\t' << mySize << '\t' << myAvgTimeMs << '\t' << myAvgSpeed
              << std::endl;
  }

  return 0;
}
