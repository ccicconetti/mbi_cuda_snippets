// Copyright (c) 2017-2022 M.B.I srl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//   CONTACT-INFO  Massimiliano Ghilardi
//
//   Postal: MBI S.r.l.
//   Via F. Squartini, 6
//   I56121 - Pisa
//   Italy
//
//   Email:  mghilardi@mbigroup.it

#include <cassert>
#include <cmath>
#include <iostream>

#include <cuda_fp16.h>

#include "chronometer.h"
#include "devicevector.h"
#include "hostvector.h"

static const size_t MIN_SIZE = 1 << 12;
static const size_t MAX_SIZE = 1 << 24;

static const size_t NUM_REPETITIONS = 4096;

#define BLOCK_SIZE (1 << 9)

// kernel to convert FP32->FP16
__global__ void KernelConvertFloat2ToHalf2(const float2* aIn, half2* aOut,
                                           const unsigned aSize) {
  const unsigned myPos = blockIdx.x * blockDim.x + threadIdx.x;
  if (myPos < aSize) {
    aOut[myPos] = __float22half2_rn(aIn[myPos]);
  }
}

// kernel to exec the multiplication with FP32
__global__ void KernelMul32(const float2* aIn1, const float2* aIn2,
                            float2* aOut, const unsigned aSize) {
  const unsigned myPos = blockIdx.x * blockDim.x + threadIdx.x;
  if (myPos < aSize) {
    const float2 myA = aIn1[myPos];
    const float2 myB = aIn2[myPos];
    // complex number multiplication
    aOut[myPos].x = fmaf(myA.x, myB.x, -myA.y * myB.y);
    aOut[myPos].y = fmaf(myA.x, myB.y, myA.y * myB.x);
  }
}

__device__ inline half2 multiply_half2(const half2 aValue1,
                                       const half2 aValue2) {
#if 0  // different semantics
  return aValue1 * aValue2;
#else  // complex number multiplication
  const half are = __low2half(aValue1), aim = __high2half(aValue1);
  const half bre = __low2half(aValue2), bim = __high2half(aValue2);

  return __halves2half2(__hfma(are, bre, __hneg(__hmul(aim, bim))),
                        __hfma(are, bim, __hmul(aim, bre)));
#endif
}

// kernel to exec the multiplication with FP16
__global__ void KernelMul16(const half2* aIn1, const half2* aIn2, half2* aOut,
                            const unsigned aSize) {
  const unsigned myPos = blockIdx.x * blockDim.x + threadIdx.x;
  if (myPos < aSize) {
    aOut[myPos] = multiply_half2(aIn1[myPos], aIn2[myPos]);
  }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

int main() {
  // select compute capability >= 6.0 (Pascal)
  cudaDeviceProp myProp;
  int myDev;

  cudaGetDevice(&myDev);
  memset(&myProp, 0, sizeof(cudaDeviceProp));
  myProp.major = 6;
  myProp.minor = 0;
  const cudaError_t myResultSetDevice = cudaChooseDevice(&myDev, &myProp);
  assert(myResultSetDevice == cudaSuccess);

  cudaDeviceProp myPropTest;
  cudaGetDeviceProperties(&myPropTest, myDev);

  std::cout << "# Compute capability: " << myPropTest.major << '.'
            << myPropTest.minor << std::endl;

  cudaSetDevice(myDev);

  // USE FP32
  std::cout << "# benchmark MUL    FP32" << std::endl;
  std::cout << "# size, time (ms), speed (elements/second)" << std::endl;

  for (size_t mySize = MIN_SIZE; mySize <= MAX_SIZE; mySize <<= 1) {
    // create space for vector on CPU
    const size_t mySizePadded = 5 * mySize;

    // Allocate CPU memory
    HostVector myHostVector(mySizePadded);
    // Allocate GPU memory
    DeviceVector<float2> myVector1(mySizePadded);
    DeviceVector<float2> myVector2(mySizePadded);
    DeviceVector<float2> myResult(mySizePadded);

    myHostVector.randomFill(5.0f);

    // copy host memory to device
    cudaMemcpy(myVector1.data(), myHostVector.data(),
               sizeof(float2) * mySizePadded, cudaMemcpyHostToDevice);
    cudaMemcpy(myVector2.data(), myHostVector.data(),
               sizeof(float2) * mySizePadded, cudaMemcpyHostToDevice);

    // wait for setup to complete
    cudaDeviceSynchronize();

    dim3 myGrid((mySize + BLOCK_SIZE - 1) / BLOCK_SIZE);
    dim3 myBlock(BLOCK_SIZE);

    Chronometer myChrono;

    for (size_t myTest = 0; myTest < NUM_REPETITIONS; ++myTest) {
      const size_t myOffset = lrand48() % (mySizePadded - mySize);

      KernelMul32<<<myGrid, myBlock>>>(myVector1.data() + myOffset,  //
                                       myVector2.data() + myOffset,  //
                                       myResult.data() + myOffset,   //
                                       mySize);
    }
    const float myElapsedTimeMs = myChrono.stop();
    const float myAvgTimeMs = myElapsedTimeMs / NUM_REPETITIONS;
    const float myAvgSpeed = mySize / myAvgTimeMs * 1e3f;
    std::cout << '\t' << mySize << '\t' << myAvgTimeMs << '\t' << myAvgSpeed
              << std::endl;
  }

  // USE FP16
  std::cout << "# benchmark MUL    FP16" << std::endl;
  std::cout << "# size, time (ms), speed (elements/second)" << std::endl;

  for (size_t mySize = MIN_SIZE; mySize <= MAX_SIZE; mySize <<= 1) {
    // allocate CPU memory for vector
    const size_t mySizePadded = 5 * mySize;

    // Allocate CPU memory
    HostVector myHostVector(mySizePadded);
    // Allocate GPU memory
    DeviceVector<float2> myVector(mySizePadded);
    DeviceVector<half2> myVector1Fp16(mySizePadded);
    DeviceVector<half2> myVector2Fp16(mySizePadded);
    DeviceVector<half2> myResultFp16(mySizePadded);

    myHostVector.randomFill(5.0f);

    // copy host memory to device
    cudaMemcpy(myVector.data(), myHostVector.data(),
               sizeof(float2) * mySizePadded, cudaMemcpyHostToDevice);

    // call kernel to convert FP32 to FP16
    {
      dim3 myGrid((mySizePadded + BLOCK_SIZE - 1) / BLOCK_SIZE);
      dim3 myBlock(BLOCK_SIZE);

      KernelConvertFloat2ToHalf2<<<myGrid, myBlock>>>(
          myVector.data(), myVector1Fp16.data(), mySizePadded);
      KernelConvertFloat2ToHalf2<<<myGrid, myBlock>>>(
          myVector.data(), myVector2Fp16.data(), mySizePadded);
    }
    // wait for setup to complete
    cudaDeviceSynchronize();

    dim3 myGrid((mySize + BLOCK_SIZE - 1) / BLOCK_SIZE);
    dim3 myBlock(BLOCK_SIZE);

    Chronometer myChrono;

    for (size_t myTest = 0; myTest < NUM_REPETITIONS; ++myTest) {
      const size_t myOffset = lrand48() % (mySizePadded - mySize);

      KernelMul16<<<myGrid, myBlock>>>(myVector1Fp16.data() + myOffset,  //
                                       myVector2Fp16.data() + myOffset,  //
                                       myResultFp16.data() + myOffset,   //
                                       mySize);
    }
    const float myElapsedTimeMs = myChrono.stop();
    const float myAvgTimeMs = myElapsedTimeMs / NUM_REPETITIONS;
    const float myAvgSpeed = mySize / myAvgTimeMs * 1e3f;
    std::cout << '\t' << mySize << '\t' << myAvgTimeMs << '\t' << myAvgSpeed
              << std::endl;
  }

  return 0;
}
