// Copyright (c) 2017-2022 M.B.I srl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//   CONTACT-INFO  Massimiliano Ghilardi
//
//   Postal: MBI S.r.l.
//   Via F. Squartini, 6
//   I56121 - Pisa
//   Italy
//
//   Email:  mghilardi@mbigroup.it

#include <cassert>
#include <iostream>

#include <cuda_bf16.h>
#include <cufft.h>
#include <cufftXt.h>

#include "chronometer.h"
#include "devicevector.h"
#include "hostvector.h"

#if defined(__CUDA_ARCH__) && __CUDA_ARCH__ < 800
#error "fft_bfloat16.cu uses nv_bfloat162, which requires __CUDA_ARCH__ >= 800"
#endif

static const size_t MIN_SIZE = 1 << 12;
static const size_t MAX_SIZE = 1 << 25;

static const size_t NUM_REPETITIONS = 4096;

#define BLOCK_SIZE (1 << 9)

// kernel to convert FP32->BFP16
__global__ void KernelConvertFloat2ToBfloat162(const float2* aIn,
                                               nv_bfloat162* aOut,
                                               const unsigned aSize) {
  const unsigned myPos = blockIdx.x * blockDim.x + threadIdx.x;
  if (myPos < aSize) {
    aOut[myPos] = __float22bfloat162_rn(aIn[myPos]);
  }
}

int main() {
  // select compute capability >= 8.0 (Ampere)
  cudaDeviceProp myProp;
  int myDev;

  cudaGetDevice(&myDev);
  memset(&myProp, 0, sizeof(cudaDeviceProp));
  myProp.major = 8;
  myProp.minor = 0;
  const auto myResultSetDevice = cudaChooseDevice(&myDev, &myProp);
  assert(myResultSetDevice == cudaSuccess);

  cudaDeviceProp myPropTest;
  cudaGetDeviceProperties(&myPropTest, myDev);

  std::cout << "# Compute capability: " << myPropTest.major << '.'
            << myPropTest.minor << std::endl;

  cudaSetDevice(myDev);

  std::cout << "# benchmark FFT BFLOAT16" << std::endl;
  std::cout << "# size, time (ms), speed (elements/second)" << std::endl;

  // FFT size = mySize
  for (long long mySize = MIN_SIZE; mySize <= MAX_SIZE; mySize <<= 1) {
    const size_t mySizePadded = mySize * 5;
    HostVector myHostVector(mySizePadded);
    DeviceVector<float2> myVector(mySizePadded);

    // create FFT plan
    cufftHandle myPlan;
    cufftResult myErr = cufftCreate(&myPlan);
    assert(myErr == CUFFT_SUCCESS);

    size_t myWorkSize = 0;
    myErr =
        cufftXtMakePlanMany(myPlan, 1, &mySize, NULL, 1, 1, CUDA_C_16BF, NULL,
                            1, 1, CUDA_C_16BF, 1, &myWorkSize, CUDA_C_16BF);
    assert(myErr == CUFFT_SUCCESS);

    myHostVector.randomFill(1e-3f);
    cudaMemcpy(myVector.data(), myHostVector.data(),
               sizeof(float2) * mySizePadded, cudaMemcpyHostToDevice);

    DeviceVector<nv_bfloat162> myVectorBfp16(mySizePadded);
    DeviceVector<nv_bfloat162> myResultBfp16(mySizePadded);

    // call kernel to convert FP32 to FP16
    {
      dim3 myGrid((mySizePadded + BLOCK_SIZE - 1) / BLOCK_SIZE);
      dim3 myBlock(BLOCK_SIZE);
      KernelConvertFloat2ToBfloat162<<<myGrid, myBlock>>>(
          myVector.data(), myVectorBfp16.data(), mySizePadded);
    }
    // wait for setup to complete
    cudaDeviceSynchronize();

    Chronometer myChrono;

    for (size_t myTest = 0; myTest < NUM_REPETITIONS; ++myTest) {
      // select random offset in the input
      const size_t myOffset = lrand48() % (mySizePadded - mySize);
      // calculate FFT transform
      const cufftResult myErr = cufftXtExec(
          myPlan,
          reinterpret_cast<cufftComplex*>(myVectorBfp16.data() + myOffset),
          reinterpret_cast<cufftComplex*>(myResultBfp16.data() + myOffset),
          CUFFT_FORWARD);
      assert(myErr == CUFFT_SUCCESS);
    }
    const float myElapsedMs = myChrono.stop();
    const float myAvgTimeMs = myElapsedMs / NUM_REPETITIONS;
    const float myAvgSpeed = mySize / myAvgTimeMs * 1e3f;
    std::cout << '\t' << mySize << '\t' << myAvgTimeMs << '\t' << myAvgSpeed
              << std::endl;

    // Destroy cuFFT context
    cufftDestroy(myPlan);
  }
  return 0;
}
