// Copyright (c) 2017-2022 M.B.I srl
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//   CONTACT-INFO  Massimiliano Ghilardi
//
//   Postal: MBI S.r.l.
//   Via F. Squartini, 6
//   I56121 - Pisa
//   Italy
//
//   Email:  mghilardi@mbigroup.it

#include <cassert>
#include <iostream>

#include <cuda_bf16.h>
#include <cufft.h>
#include <cufftXt.h>

#include "chronometer.h"
#include "devicevector.h"
#include "hostvector.h"

#if defined(__CUDA_ARCH__) && __CUDA_ARCH__ < 800
#error "fft_bfloat16.cu uses nv_bfloat162, which requires __CUDA_ARCH__ >= 800"
#endif

static const size_t MIN_SIZE = 1 << 10;
static const size_t MAX_SIZE = 1 << 20;

static const size_t NUM_REPETITIONS = 4096;

#define BLOCK_SIZE (1 << 9)

// kernel to convert FP32->BFP16
__global__ void KernelConvertFloat2ToBfloat162(const float2* aIn,
                                               nv_bfloat162* aOut,
                                               const unsigned aSize) {
  const unsigned myPos = blockIdx.x * blockDim.x + threadIdx.x;
  if (myPos < aSize) {
    aOut[myPos] = __float22bfloat162_rn(aIn[myPos]);
  }
}

int main() {
  // select compute capability >= 8.0 (Ampere)
  cudaDeviceProp myProp;
  int myDev;

  cudaGetDevice(&myDev);
  memset(&myProp, 0, sizeof(cudaDeviceProp));
  myProp.major = 8;
  myProp.minor = 0;
  const auto myResultSetDevice = cudaChooseDevice(&myDev, &myProp);
  assert(myResultSetDevice == cudaSuccess);

  cudaDeviceProp myPropTest;
  cudaGetDeviceProperties(&myPropTest, myDev);

  std::cout << "# Compute capability: " << myPropTest.major << '.'
            << myPropTest.minor << std::endl;

  cudaSetDevice(myDev);

  const size_t myY = 120;
  const size_t myVectorLen = MAX_SIZE * myY;

  HostVector myHostVector(myVectorLen);
  DeviceVector<float2> myVector(myVectorLen);
  DeviceVector<nv_bfloat162> myVectorBfp16(myVectorLen);
  DeviceVector<nv_bfloat162> myResultBfp16(myVectorLen);

  std::cout << "# benchmark FFT_MULTI BFLOAT16, batch size = " << myY
            << std::endl;
  std::cout << "# FFT size, time (ms), speed (elements/second)" << std::endl;

  // FFT size = mySize
  for (long long mySize = MIN_SIZE; mySize <= MAX_SIZE; mySize <<= 1) {
    const size_t myRingSize = MAX_SIZE / mySize;
    int myStrideX = 1;
    int myStrideY = mySize;

    // create FFT plan
    cufftHandle myPlan;
    cufftResult myErr = cufftCreate(&myPlan);
    assert(myErr == CUFFT_SUCCESS);

    long long myEmbed = mySize;
    size_t myWorkSize = 0;
    myErr = cufftXtMakePlanMany(myPlan,
                                1,             // rank
                                &mySize,       // FFT size
                                &myEmbed,      // must be {aLength} for 1-D FFT
                                myStrideX,     // input vector stride
                                myStrideY,     // input batch distance
                                CUDA_C_16BF,   // input type = nv_bfloat162
                                &myEmbed,      // must be {aLength} for 1-D FFT
                                myStrideX,     // output vector stride
                                myStrideY,     // output batch distance
                                CUDA_C_16BF,   // output type = nv_bfloat162
                                myY,           // batch size = # FFT
                                &myWorkSize,   // work area size
                                CUDA_C_16BF);  // algo type = nv_bfloat162
    assert(myErr == CUFFT_SUCCESS);

    myHostVector.randomFill(1e-3f);
    cudaMemcpy(myVector.data(), myHostVector.data(), sizeof(float2) * mySize,
               cudaMemcpyHostToDevice);

    // call kernel to convert FP32 to BFLOAT16
    {
      dim3 myGrid((myVectorLen + BLOCK_SIZE - 1) / BLOCK_SIZE);
      dim3 myBlock(BLOCK_SIZE);
      KernelConvertFloat2ToBfloat162<<<myGrid, myBlock>>>(
          myVector.data(), myVectorBfp16.data(), myVectorLen);
    }
    // wait for setup to complete
    cudaDeviceSynchronize();

    Chronometer myChrono;

    for (size_t myTest = 0; myTest < NUM_REPETITIONS; ++myTest) {
      // select random offset in the input
      const size_t myOffset =
          myRingSize == 1 ? 0 : lrand48() % (mySize * myY * (myRingSize - 1));
      // calculate FFT transform
      const cufftResult myErr = cufftXtExec(myPlan,                           //
                                            myVectorBfp16.data() + myOffset,  //
                                            myResultBfp16.data() + myOffset,  //
                                            CUFFT_FORWARD);
      assert(myErr == CUFFT_SUCCESS);
    }
    const float myElapsedMs = myChrono.stop();

    const float myAvgTimeMs = myElapsedMs / NUM_REPETITIONS;
    const float myAvgSpeed = (mySize * myY) / myAvgTimeMs * 1e3f;
    std::cout << '\t' << mySize << '\t' << myAvgTimeMs << '\t' << myAvgSpeed
              << std::endl;

    // Destroy cuFFT context
    cufftDestroy(myPlan);
  }

  return 0;
}
