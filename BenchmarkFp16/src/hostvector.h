#pragma once

#include <cstdlib>  // drand48()
#include <vector>   // std::vector<>

#include <vector_types.h>  // CUDA float2

class HostVector : public std::vector<float2> {
 public:
  explicit HostVector(const size_t aSize) : std::vector<float2>(aSize) {}

  // fill with random data in [-aMax, aMax)
  void randomFill(const float aMax) {
    const size_t mySize = size();
    for (size_t i = 0; i < mySize; i++) {
      (*this)[i].x = aMax * (static_cast<float>(drand48()) * 2.0f - 1.0f);
      (*this)[i].y = aMax * (static_cast<float>(drand48()) * 2.0f - 1.0f);
    }
  }
};
