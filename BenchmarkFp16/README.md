## Synopsis

The project contains 3 CUDA files in order to check the performance of 16-bit Floating Point (FP16) data operations
offered with GPU architectures >= 6.0 (Pascal).

The analyzed operations are the following:

* FFT realized by using the cuFFT library
* Reduction realized by using the algorithm suggested by NVidia in order to optimize the parallel reduction in CUDA (reduction_kernel.cu)
* Multiplication

The performance have been measured by using CUDA Events.
The vectors are complex.


## Compilation

Below the command used on our system in order to compile the files (e.g., on a RTX A6000):

* /usr/local/cuda/bin/nvcc -O3 --generate-code arch=compute_86,code=sm_86 --default-stream per-thread -I/usr/local/cuda/include/  mul.cu -o mul
* /usr/local/cuda/bin/nvcc -O3 --generate-code arch=compute_86,code=sm_86 --default-stream per-thread -I/usr/local/cuda/include/  reduce.cu -o reduce

* /usr/local/cuda/bin/nvcc -O3 --generate-code arch=compute_86,code=sm_86 --default-stream per-thread -I/usr/local/cuda/include/ -L/usr/local/cuda/lib64/ -lcufft fft.cu -o fft
* /usr/local/cuda/bin/nvcc -O3 --generate-code arch=compute_86,code=sm_86 --default-stream per-thread -I/usr/local/cuda/include/ -L/usr/local/cuda/lib64/ -lcufft fft_bfloat16.cu -o fft_bfloat16

* /usr/local/cuda/bin/nvcc -O3 --generate-code arch=compute_86,code=sm_86 --default-stream per-thread -I/usr/local/cuda/include/ -L/usr/local/cuda/lib64/ -lcufft fft_multi.cu -o fft_multi
* /usr/local/cuda/bin/nvcc -O3 --generate-code arch=compute_86,code=sm_86 --default-stream per-thread -I/usr/local/cuda/include/ -L/usr/local/cuda/lib64/ -lcufft fft_multi_bfloat16.cu -o fft_multi_bfloat16
